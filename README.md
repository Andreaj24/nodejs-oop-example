

# Installation

```
npm install

npm start

````

#API

###Get all box objects

```
GET /box

```


###Get one box from id

```

GET /box/:id


```

###Delete all box 

```

DELETE /box/:id
```


###Delete one box from id

```
DELETE /box/:id

```

###Create nex box 

```
POST /box

```

Example obj

```

{
    "created_at" : "23142015",
    "customer_name" :"Andrea giglio",
    "address" : "Via respighi",
    "items" :["uno","due","tre"]
}


```

###Find

```
POST /find/:filter

```

Filters

```
POST /find/created_at
POST /find/customer_name
POST /find/address
POST /find/id


```


###Extras

you can call this API to save the current state of the object in memory in the CSV file

```
POST /save

```

##Example file for Api [postman]

```
https://www.getpostman.com/collections/53ed53fe5eea6c9004a1

```

[ChromeTool link](https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm)


Info:

Andrea Giglio, 
andrea-giglio@live.it