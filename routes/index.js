var express = require('express'),
    router = express.Router();
var BoxContainer;

require('../dataSource.js').init(function (boxContainer) {
    BoxContainer = boxContainer;
});

/* GET home page. */
router.get('/box', function (req, res) {
    BoxContainer.getAll(function (err, obj) {
        if (err) return res.status(500).json({
            "msg": "Get all box error",
            "error": err
        });
        return res.status(200).json({
            "success": true,
            "data": obj
        });
    });
});

router.get('/box/:id', function (req, res) {
    BoxContainer.getById(req.params.id, function (err, obj) {
        if (err) return res.status(500).json({
            "msg": "Box creation error"
        })
        if (!obj) return res.status(404).json({
            "msg": "Box not found"
        })
        return res.status(200).json({
            "success": true,
            data: obj
        });
    });
});

router.delete('/box/:id', function (req, res) {
    BoxContainer.delete(req.params.id, function (err, obj) {
        if (err) return res.status(500).json({
            "msg": "Box delete error"
        })
        return res.status(200).json({
            "success": true
        });
    });
});
router.delete('/box', function (req, res) {
    BoxContainer.deleteAll(function (err, obj) {
        if (err) return res.status(500).json({
            "msg": "Box delete error"
        })
        return res.status(200).json({
            "success": true
        });
    });
});
router.post('/box', function (req, res) {
    BoxContainer.add(req.body, function (err, obj) {
        if (err) return res.status(500).json({
            "msg": "Box creation error"
        })
        return res.status(200).json({
            "success": true,
            "data": obj
        });
    });
});
router.get('/find/:filter', function (req, res) {
    BoxContainer.find(req.params.filter, function (err, obj) {
        if (err) return res.status(500).json({
            "msg": "Box creation error"
        })
        if (!obj) return res.status(404).json({
            "msg": "Box not found"
        })
        return res.status(200).json({
            "success": true,
            "data": obj
        });
    });
});

// Fet the obj in csv format like the orignal file 
router.post('/save', function (req, res) {
    BoxContainer.saveToCsv(function (err, obj) {
        if (err) return res.status(500).json({
            "msg": "Box saving error",
            "err": err
        })
        return res.status(200).send(obj);
    });
});

module.exports = router;