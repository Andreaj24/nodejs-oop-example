var fs = require('fs');

var baseArray = [];

function Box(line, obj) {
    if (!obj) {
        if (line.split(',').length != 5) return {
            "Error": "no valid line in Box Costructor"
        };
        //    console.log("|---> LINE --->" + line.split(','));
        // console.log("|---> LINE --->" + line.split(',').length)
        line = line.split(',');
        this.id = line[0];
        this.created_at = line[1];
        this.customer_name = line[2];
        this.address = line[3];
        this.items = line[4].split('/');
    } else {
        this.id = obj.id;
        this.created_at = obj.created_at;
        this.customer_name = obj.customer_name;
        this.address = obj.address;
        this.items = obj.items;
    }
}

Box.prototype = {
    constructor: Box,
    toCsvLine: function (separator) {
        var _separator = separator ? separator : ',';
        return this.id + _separator + " " + this.created_at + _separator + " " + this.customer_name + _separator + " " + this.address + _separator + " " + this.items.join('/')
    }
}

function BoxContainer(lines) {
    this.boxs = [];
    for (var i in lines) {
        //        console.log("LINES ---> " + lines[i])
        //        console.log("BOX ---> " + JSON.stringify(new Box(lines[i])))
        this.boxs.push(new Box(lines[i]))
    }
}

BoxContainer.prototype = {
    constructor: BoxContainer,
    saveToCsv: function (callback) {
        var fileString;
        for (var i in this.boxs) {
            fileString += "\n" + this.baseArray[i].toCsvLine();
        }
        var path = './example_data.csv',
            buffer = new Buffer(fileString);

        fs.open(path, 'w', function (err, fd) {
            if (err) {
                console.log("ERROR" + err)
                return callback(error)
            } else {
                fs.write(fd, buffer, 0, buffer.length, null, function (err) {
                    if (err) return callback(err)
                    fs.close(fd, function () {
                        console.log('file written');
                        return callback(null, fileString)
                    })
                });
            }
        });
    },
    getAll: function (callback) {
        callback(null, this.boxs);
    },
    getById: function (id, callback) {
        var obj = undefined;
        for (var i in this.boxs) {
            console.log("-ID->" + this.boxs[i].id)
            if (this.boxs[i].id == id) {
                obj = this.boxs[i];
                break;
            }
        }
        return callback(null, obj);
    },
    delete: function (id, callback) {
        for (var i in this.boxs) {
            console.log("-ID->" + this.boxs[i].id)
            if (this.boxs[i].id == id) {
                this.boxs.splice(i, 1);
                break;
            }
        }
        callback(null, true);
    },
    deleteAll: function (callback) {
        this.boxs = [];
        callback(null, true);
    },
    add: function (obj, callback) {
        obj.id = this.boxs.length;
        var o_b = new Box(null, obj);
        this.boxs.push(o_b);
        callback(null, o_b);
    },
    find: function (filter, callback) {
        var obj = undefined;

        function compare(a, b) {
            if (a[filter + ""] < b[filter + ""])
                return -1;
            if (a[filter + ""] > b[filter + ""])
                return 1;
            return 0;
        }
        return callback(null, this.boxs.sort(compare));
    }
}

exports.init = function (callback) {
    fs.readFile(__dirname + '/example_data.csv', 'utf8', function (err, data) {
        if (err) throw err;
        var lines = data.split(/\r?\n/);
        return callback(new BoxContainer(lines));
    });
};